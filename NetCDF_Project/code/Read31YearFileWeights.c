/**
Program that takes weighted file and applies it to all HRR IDs specified in the file
by using NetCDF to gather hourly variables for all 31 years (1979 - 2009).

Created by Andrew Perry
FM Global
**/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "C:/Users/perryad/Desktop/Projects/netCDF 4.3.1.1/include/netcdf.h"

#define LAT 576 // 576
#define LON 1152 // 1152
#define ERRCODE 2
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}

// Function that creates 2d array
float** make2dArray(int rows, int cols)
{
	float **my2dArray;
	my2dArray = (float**)malloc(sizeof(float*)*rows);
	int i;
	for (i = 0; i < rows; i++)
	{
		my2dArray[i] = (float*)malloc(sizeof(float)*cols);
	}
	return my2dArray;
}

// Destroy 2d array
void destroy2dArray(float **array2d, int rows)
{
	int destroyIter;
	for (destroyIter = 0; destroyIter < rows; destroyIter++)
	{
		free(array2d[destroyIter]);
	}
	free(array2d);
}

// Open weighted file and put into an array
float** getWeights(char *inputFile, int *lineMaxTotal)
{
	// Open master folder with all catchments
	FILE *weightFile = fopen(inputFile, "r");
	// Get first line to determine number of lines/iterations
	char outerLine[200];
	fgets(outerLine, sizeof outerLine, weightFile);

	int lineMax;
	sscanf(outerLine, "%d", &lineMax);
	*lineMaxTotal = lineMax;
	printf("Line max = %d\n", lineMax);

	// Call function to create 2dArray
	float **arrayOfWeights = make2dArray(lineMax, 10);

	int lineIter;
	for (lineIter = 0; lineIter < lineMax; lineIter++)
	{
		char innerLine[200];
		fgets(innerLine, sizeof innerLine, weightFile);

		int catchmentID;
		int ij1, ij2, ij3, ij4;
		float f_ij1, f_ij2, f_ij3, f_ij4;
		float weight1, weight2, weight3, weight4;
		sscanf(innerLine, "%d %d %f %d %f %d %f %d %f", &catchmentID, &ij1, &weight1, &ij2, &weight2, &ij3, &weight3, &ij4, &weight4);
		// Need to convert ints to float for array, will be converted back
		f_ij1 = (float)ij1;
		f_ij2 = (float)ij2;
		f_ij3 = (float)ij3;
		f_ij4 = (float)ij4;

		arrayOfWeights[lineIter][0] = catchmentID;
		arrayOfWeights[lineIter][1] = f_ij1;
		arrayOfWeights[lineIter][2] = weight1;
		arrayOfWeights[lineIter][3] = f_ij2;
		arrayOfWeights[lineIter][4] = weight2;
		arrayOfWeights[lineIter][5] = f_ij3;
		arrayOfWeights[lineIter][6] = weight3;
		arrayOfWeights[lineIter][7] = f_ij4;
		arrayOfWeights[lineIter][8] = weight4;
	}
	fclose(weightFile); // Close the file connection

	// Determine the number of points affecting a catchment and
	// put that number at the end of the array at index [9]
	int arrayIter;
	for (arrayIter = 0; arrayIter < lineMax; arrayIter++)
	{
		float length;
		if ((int)arrayOfWeights[arrayIter][7] != -1)
		{
			length = 4.0;
			arrayOfWeights[arrayIter][9] = length;
		}
		else if ((int)arrayOfWeights[arrayIter][5] != -1)
		{
			length = 3.0;
			arrayOfWeights[arrayIter][9] = length;
		}
		else if ((int)arrayOfWeights[arrayIter][3] != -1)
		{
			length = 2.0;
			arrayOfWeights[arrayIter][9] = length;
		}
		else
		{
			length = 1.0;
			arrayOfWeights[arrayIter][9] = length;
		}
	}
	return arrayOfWeights;
}

// Get the netcdf starting position as well as the X and Y count
// by finding minimum and maximum X and Y values
void getNetCDFArea(float** arrayOfWeights, int lineMax, int* ijBoxId, int* xCount, int* yCount)
{
    int tempX, tempY;
    int minX = 9999999;
    int minY = 9999999;
    int maxX = 0;
    int maxY = 0;
    int arrayIter;
    // Loop through array and loop through array inside current index
	for (arrayIter = 0; arrayIter < lineMax; arrayIter++)
	{
	    //1,3,5,7
	    int indexId;
	    for (indexId = 1; indexId < 8; indexId+=2)
        {
            if ((int)arrayOfWeights[arrayIter][indexId] != -1)
            {
                calculateXY((int)arrayOfWeights[arrayIter][indexId], &tempX, &tempY);
                if (tempX > maxX)
                {
                    maxX = tempX;
                }
                else if (tempX < minX)
                {
                    minX = tempX;
                }
                if (tempY > maxY)
                {
                    maxY = tempY;
                }
                else if (tempY < minY)
                {
                    minY = tempY;
                }
            }
        }
	}
	*xCount = maxX - minX + 1;
	*yCount = maxY - minY + 1;
	*ijBoxId = getBoxId(minX, minY);
	printf("minX:\t%d\tmaxX:\t%d\tminY:\t%d\tmaxY:\t%d\n", minX, maxX, minY, maxY);
}

// Return the IJ Box value for lower left corner
int getBoxId(int minX, int minY)
{
    int ij;
    ij = (((minY - 1) * 1152) + minX);
    return ij;
}

// Calculate X and Y value from ij box
void calculateXY(int ijBoxId, int *xVal, int *yVal)
{
    if (ijBoxId <= 1152)
    {
        *xVal = ijBoxId;
        *yVal = 1;
    }
    else
    {   if (ijBoxId % 1152 == 0)
        {
            *xVal = 1152;
            *yVal = ijBoxId / 1152;
        }
        else
        {
            *xVal = ijBoxId % 1152;
            *yVal = (ijBoxId / 1152) + 1;
        }
    }
}

// Function that returns hours in a year.
int checkYear(int year)
{
    int hoursInYear;
    if (year % 4 == 0)
    {
        hoursInYear = 8784;
    }
    else
    {
        hoursInYear = 8760;
    }
    return hoursInYear;
}

// Calculate the hour number to start based on year
int hourStart(int year)
{
    int leapCalc = (year - 1979 + 2) / 4;
    int startHour = (leapCalc * 8784) + ((year - 1979 - leapCalc) * 8760);
    return startHour;
}

// Calculate total hours to be run in one session
void totalHours(int year, int totalYears, int* beginHour, int* hourAmount)
{
    int leapCalc = (year - 1979 + 2) / 4;
    int startHour = (leapCalc * 8784) + ((year - 1979 - leapCalc) * 8760);
    int leapCalc2 = (year + totalYears - 1979 + 2) / 4;
    int startHour2 = (leapCalc2 * 8784) + ((year + totalYears - 1979 - leapCalc2) * 8760);
    int totalHours = startHour2 - startHour;
    *beginHour = startHour;
    *hourAmount = totalHours;
}

// Print out information of binary file so it can be used to extract data
void printReadMe(char* outputPath, float** weightArray, int lineMaxTotal, int hourVal)
{
		char outputReadMe[256];
		strcpy(outputReadMe, outputPath);
		outputReadMe[strlen(outputReadMe)-4] = '\0';
		printf("outputReadMe = %s\n", outputReadMe);
		strcat(outputReadMe, "_readme.txt");
        FILE *outputRead;
        outputRead = fopen(outputReadMe, "w");
        fprintf(outputRead, "%d\t%d\n", lineMaxTotal, hourVal);
        int currentHRR;
        for (currentHRR = 0; currentHRR < lineMaxTotal; currentHRR++)
        {
            fprintf(outputRead, "%d\n", (int)weightArray[currentHRR][0]);
        }
        fclose(outputRead);
}

// Convert to 8bit integer and map
uint8_t convertToByte(float originalVal, int conversion)
{
    uint8_t convertedVal = 0;
    if (conversion == 0)
    {
        // Convert from Kelvin and then add 100 to keep values between 0 and 255
        float tempVal = round(originalVal - 272.15 + 100);
        if (tempVal > 255 || tempVal < 0)
        {
            printf("Temp Value out of bounds!\n");
        }
        convertedVal = (uint8_t)tempVal;
    }
    else if (conversion == 1)
    {
        // Multiply humidity value by 1000 to keep values between 0 and 255
        float tempVal = round(originalVal * 1000);
        if (tempVal > 255 || tempVal < 0)
        {
            printf("Humidity Value out of bounds!\n");
        }
        convertedVal = (uint8_t)tempVal;
    }
    else if (conversion == 2)
    {
        // No initial conversion required, just from int32 to int8
        if (originalVal > 255 || originalVal < 0)
        {
            printf("Wind Value out of bounds!\n");
        }
        convertedVal = (uint8_t)originalVal;
    }
    else
    {
        printf("THERE WAS AN ERROR!\n");
    }
    return convertedVal;
}

// Loop through weight array and apply weights to catchments using netcdf data
void weightsOnNetCDF(char* outputPath, float* hourlyData, float** weightArray, int lineMaxTotal, int conversion, int hourVal, int xVal, int yVal, int numRows, int numCols)
{
    printf("Calculating weights from NetCDF.\nNum rows:\t%d\nNum cols:\t%d\n", numRows, numCols);
    if (strstr(outputPath, "bin") != NULL)
    {
        printReadMe(outputPath, weightArray, lineMaxTotal, hourVal);
        FILE *outputFile;
        outputFile = fopen(outputPath, "wb");
        int weightPosition;
        for (weightPosition = 0; weightPosition < lineMaxTotal; weightPosition++)
        {
            if ((int)weightArray[weightPosition][9] == 4)
            {
                int x1, x2, x3, x4, y1, y2, y3, y4;
                float weight1, weight2, weight3, weight4;
                float totalWeight;
                // Return x and y values for each ijbox id to locate within netcdf array
                calculateXY((int)weightArray[weightPosition][1], &x1, &y1);
                calculateXY((int)weightArray[weightPosition][3], &x2, &y2);
                calculateXY((int)weightArray[weightPosition][5], &x3, &y3);
                calculateXY((int)weightArray[weightPosition][7], &x4, &y4);

                int hourCount;
                for (hourCount = 0; hourCount < hourVal; hourCount++)
                {
                    // Weights are spread every other position in the array 2, 4, 6, 8
                    // Use x1-x4 and y1-y4 to jump through parts of 1D array
                    weight1 = hourlyData[(hourCount * numRows * numCols) + ((y1 - yVal) * numCols) + (x1 - xVal)] * weightArray[weightPosition][2];
                    weight2 = hourlyData[(hourCount * numRows * numCols) + ((y2 - yVal) * numCols) + (x2 - xVal)] * weightArray[weightPosition][4];
                    weight3 = hourlyData[(hourCount * numRows * numCols) + ((y3 - yVal) * numCols) + (x3 - xVal)] * weightArray[weightPosition][6];
                    weight4 = hourlyData[(hourCount * numRows * numCols) + ((y4 - yVal) * numCols) + (x4 - xVal)] * weightArray[weightPosition][8];
                    totalWeight = weight1 + weight2 + weight3 + weight4;
                    uint8_t convertedWeight = convertToByte(totalWeight, conversion);
                    fwrite(&convertedWeight, sizeof(uint8_t), sizeof(convertedWeight)/sizeof(uint8_t), outputFile);
                }
            }
            else if ((int)weightArray[weightPosition][9] == 3)
            {
                int x1, x2, x3, y1, y2, y3;
                float weight1, weight2, weight3;
                float totalWeight;
                calculateXY((int)weightArray[weightPosition][1], &x1, &y1);
                calculateXY((int)weightArray[weightPosition][3], &x2, &y2);
                calculateXY((int)weightArray[weightPosition][5], &x3, &y3);

                int hourCount;
                for (hourCount = 0; hourCount < hourVal; hourCount++)
                {
                    weight1 = hourlyData[(hourCount * numRows * numCols) + ((y1 - yVal) * numCols) + (x1 - xVal)] * weightArray[weightPosition][2];
                    weight2 = hourlyData[(hourCount * numRows * numCols) + ((y2 - yVal) * numCols) + (x2 - xVal)] * weightArray[weightPosition][4];
                    weight3 = hourlyData[(hourCount * numRows * numCols) + ((y3 - yVal) * numCols) + (x3 - xVal)] * weightArray[weightPosition][6];
                    totalWeight = weight1 + weight2 + weight3;
                    uint8_t convertedWeight = convertToByte(totalWeight, conversion);
                    fwrite(&convertedWeight, sizeof(uint8_t), sizeof(convertedWeight)/sizeof(uint8_t), outputFile);
                }
            }
            else if ((int)weightArray[weightPosition][9] == 2)
            {
                int x1, x2, y1, y2;
                float weight1, weight2, weight3;
                float totalWeight;
                calculateXY((int)weightArray[weightPosition][1], &x1, &y1);
                calculateXY((int)weightArray[weightPosition][3], &x2, &y2);

                int hourCount;
                for (hourCount = 0; hourCount < hourVal; hourCount++)
                {
                    weight1 = hourlyData[(hourCount * numRows * numCols) + ((y1 - yVal) * numCols) + (x1 - xVal)] * weightArray[weightPosition][2];
                    weight2 = hourlyData[(hourCount * numRows * numCols) + ((y2 - yVal) * numCols) + (x2 - xVal)] * weightArray[weightPosition][4];
                    totalWeight = weight1 + weight2;
                    uint8_t convertedWeight = convertToByte(totalWeight, conversion);
                    fwrite(&convertedWeight, sizeof(uint8_t), sizeof(convertedWeight)/sizeof(uint8_t), outputFile);
                }
            }
            else if ((int)weightArray[weightPosition][9] == 1)
            {
                int x1, y1;
                float weight1, weight2, weight3;
                float totalWeight;
                calculateXY((int)weightArray[weightPosition][1], &x1, &y1);

                int hourCount;
                for (hourCount = 0; hourCount < hourVal; hourCount++)
                {
                    weight1 = hourlyData[(hourCount * numRows * numCols) + ((y1 - yVal) * numCols) + (x1 - xVal)] * weightArray[weightPosition][2];
                    totalWeight = weight1;
                    uint8_t convertedWeight = convertToByte(totalWeight, conversion);
                    fwrite(&convertedWeight, sizeof(uint8_t), sizeof(convertedWeight)/sizeof(uint8_t), outputFile);
                }
            }
        }
        fclose(outputFile);
    }
    else
    {
        printf("Please use \".bin\".");
    }
}

// Create subarray from NetCDF and then use weight array to output hourly weighted data
void getArrays(char* inputNetCDF, char*outputPath, float** weightArray, int lineMaxTotal, int year, int amountOfYears, int ijId, int numRows, int numCols)
{
    // Retrieve hours in year
    int xVal;
    int yVal;
    calculateXY(ijId, &xVal, &yVal);
    //int startHour = hourStart(year);
    int beginHour;
    int hourVal;
    printf("Year:\t%d\tAmount of Years:\t%d\n", year, amountOfYears);
    totalHours(year, amountOfYears, &beginHour, &hourVal);
    printf("Start hour:\t%d\nNumber of hours:\t%d\n", beginHour, hourVal);

    // Initialize variables
    int ncid;
    int m_varid;
    size_t start[] = {beginHour,yVal-1,xVal-1}; // 0,0 is lower corner, not 1,1
    size_t count[] = {hourVal,numRows,numCols};
    float* hourlyData = malloc(sizeof(float) * hourVal * numRows * numCols);

    nc_open(inputNetCDF, NC_NOWRITE, &ncid);

    int conversion;
    if (strstr(inputNetCDF, "tmp2m") != NULL)
    {
        conversion = 0;
        nc_inq_varid(ncid, "TMP_2maboveground", &m_varid);
    }
    else if (strstr(inputNetCDF, "q2m") != NULL)
    {
        conversion = 1;
        nc_inq_varid(ncid, "SPFH_2maboveground", &m_varid);
    }
    else if (strstr(inputNetCDF, "wndmag10m") != NULL)
    {
        conversion = 2;
        nc_inq_varid(ncid, "wind_magnitude", &m_varid);
    }

    nc_get_vara_float(ncid, m_varid, start, count, hourlyData);
    nc_close(ncid);

    weightsOnNetCDF(outputPath, hourlyData, weightArray, lineMaxTotal, conversion, hourVal, xVal, yVal, numRows, numCols);

    free(hourlyData);
}

int main(int argc, char **argv)
{
    clock_t begin, end;
    double time_spent;
    begin = clock();
    if (argc < 6)
    {
        printf("You did not give enough arguments.\nPlease give -weightfile -inputfile -outputfile -year -amountOfYears.\n");
    }
    char *inputWeightFile = argv[1];
    char *inputNetCDF = argv[2];
    char *outputPath = argv[3];
    int year;
    sscanf(argv[4], "%d", &year);
    int amountOfYears;
    sscanf(argv[5], "%d", &amountOfYears);
    int lineMaxTotal;
    int xCount, yCount;
    int ijBoxId;
	float **weightArray = getWeights(inputWeightFile, &lineMaxTotal);
	getNetCDFArea(weightArray, lineMaxTotal, &ijBoxId, &xCount, &yCount);
	getArrays(inputNetCDF, outputPath, weightArray, lineMaxTotal, year, amountOfYears, ijBoxId, yCount, xCount);
	destroy2dArray(weightArray, lineMaxTotal);
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("It took %f seconds...\n", time_spent);
    return 1;
}
