#-------------------------------------------------------------------------------
# Name:        DEMSmoothing.Cread3dLinesPostGIS
# Purpose:
#
# Author:      Andrew Perry
#
# Created:     Feb 4, 2014
# Copyright:   (c) FM Global 2013
#-------------------------------------------------------------------------------
import psycopg2
import csv
import shutil
import os
import time
import subprocess
import arcview
import arcpy

arcpy.env.overwriteOutput = True

class PostGIS(object):
    """Class that creates a cursor and utilizes PostGIS to run spatial analysis."""
    
    def __init__(self, dbname, user, host, password, schema):
        """Instantiate the object by creating cursor and making database connection."""
        
        self.dbname = dbname
        self.user = user
        self.host = host
        self.password = password
        self.schema = schema
        self.canCalculateHeight = False
        
        try:
            self.conn = psycopg2.connect("dbname='{0}' user='{1}' host='{2}' password='{3}'".format(self.dbname, self.user,
                                                                                      self.host, self.password))
            self.conn.set_isolation_level(0)
        except:
            print "Unable to connect to the database."
            
        self.cursor = self.conn.cursor()

    def getHeight(self, slope, m, q, b):
        """Calculate height to be applied to DEM adjustment."""
        
        height0 = pow(((1.3 * q * m)/(pow(slope,(1/2)) * b)),(3/5))
        heightDifference = 99999
        while heightDifference >= 0.01:
            heighti = (pow((q * m), (3/5.0)) * pow((2*height0 + b), (2/5.0))) / (b * pow(slope,(3/10.0)))
            heightDifference = heighti - height0
            height0 = heighti
        return heighti

    def calculateHeight(self, csvTable):
        """Loop through table and calculate height values."""
        
        print "Calculating height..."
        
        listOfRows = []
        
        # Loop through and calculate height
        csvFile = open(csvTable, "rb")
        csvReader = csv.reader(csvFile)
        i = 0
        for row in csvReader:
            if i != 0:
                reachCode = row[0]
                slope = float(row[1])
                m = float(row[2])
                q = float(row[3])
                b = float(row[4])
                densify = row[5]
                finalHeight = self.getHeight(slope, m, q, b)
                listOfRows.append([reachCode, str(slope), str(m), str(q), str(b), densify, str(finalHeight)])
            i += 1
        csvFile.close()
        
        # Loop through and make new table
        csvFile = open(csvTable, "wb")
        csvWriter = csv.writer(csvFile, delimiter=",")
        csvWriter.writerow(["reach_code", "slope", "m", "q", "b", "densify", "height"])
        for row in listOfRows:
            csvWriter.writerow(row)
        csvFile.close()
        
    def moveTable(self, csvTable):
        """Move table from folder to Postgres."""
        
        print "Moving table..."
        
        postLoc = r"C:\temp\%s" % (os.path.split(csvTable)[1])
        shutil.copy(csvTable, postLoc)
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.inputs;
                CREATE TABLE {0}.inputs 
                (
                    reach_code text,
                    slope double precision,
                    m double precision,
                    q double precision,
                    b double precision,
                    densify double precision,
                    height double precision
                );
                COPY {0}.inputs
                FROM '{1}'
                WITH DELIMITER ',' 
                CSV HEADER;
                END;""".format(self.schema, postLoc)
        self.cursor.execute(SQL)
    
    def createLinesAndBuffers(self):
        """Create buffers and then lines from buffered rivers."""
        
        print "Creating buffers and intersected lines..."
        
        # Create buffers around each of the separate reaches
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.buffers;
                CREATE TABLE {0}.buffers AS
                WITH riv_select AS
                (
                    SELECT a.reachcode, b.slope, b.m, b.q, b.b, b.densify, b.height, a.geom
                    FROM {0}.rivers a LEFT JOIN {0}.inputs b
                    ON a.reachcode = b.reach_code
                )
                SELECT reachcode, slope, m, q, b, densify, height, ST_Buffer(geom, b/2) AS geom
                FROM riv_select;
                CREATE INDEX buffers_gist ON {0}.buffers USING GIST(geom);
                END;""".format(self.schema)
        self.cursor.execute(SQL)
        
        # Create lines that are inside the buffer   
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.clipped_lines;
                CREATE TABLE {0}.clipped_lines AS
                SELECT xs.gid, xs.reachcode, ST_Intersection(xs.geom, bfrs.geom) as geom
                FROM {0}.xs_cut_lines xs, {0}.buffers bfrs
                WHERE xs.reachcode = bfrs.reachcode AND ST_Intersects(xs.geom, bfrs.geom);
                CREATE INDEX clipped_lines_gist ON {0}.clipped_lines USING GIST(geom);
                END;""".format(self.schema)
        self.cursor.execute(SQL)
        
        # Create lines that are outside the buffer
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.erased_lines;
                CREATE TABLE {0}.erased_lines AS
                SELECT xs.gid, xs.reachcode, ST_Difference(xs.geom, bfrs.geom) as geom
                FROM {0}.xs_cut_lines xs, {0}.buffers bfrs
                WHERE xs.reachcode = bfrs.reachcode AND ST_Intersects(xs.geom, bfrs.geom);
                CREATE INDEX erased_lines_gist ON {0}.erased_lines USING GIST(geom);
                END;""".format(self.schema)
        self.cursor.execute(SQL)
        
    def createPoints(self, deleteOutsidePoints=True):
        """Create points along both types of lines."""
        
        print "Create points on line to make 3d line..."
        
        # Create outside points
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.merged_points;
                CREATE TABLE {0}.merged_points AS
                WITH dump AS
                (
                    SELECT gid, reachcode, ST_DumpPoints(ST_Segmentize(geom, 10.0)) AS dp
                    FROM {0}.erased_lines
                )
                SELECT gid, reachcode, 0 AS flag, (dp).path, (dp).geom
                FROM dump;
                END;""".format(self.schema)
        self.cursor.execute(SQL)
        
        # Create inside 2 points to delete other points
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.inner_2_points;
                CREATE TABLE {0}.inner_2_points AS
                WITH dump AS
                (
                    SELECT gid, reachcode, ST_DumpPoints(geom) AS dp
                    FROM {0}.clipped_lines
                )
                SELECT gid, reachcode, 1 AS flag, (dp).path, (dp).geom
                FROM dump;
                END;""".format(self.schema)
        self.cursor.execute(SQL)
        
        # Create temporary inside points
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.temp_inside;
                CREATE TABLE {0}.temp_inside AS
                WITH join_table AS
                (
                    SELECT a.gid, a.reachcode, b.densify, a.geom
                    FROM {0}.clipped_lines a LEFT JOIN {0}.inputs b
                    ON a.reachcode = b.reach_code
                ),
                dump AS
                (
                    SELECT gid, reachcode, ST_DumpPoints(ST_Segmentize(geom, densify)) AS dp
                    FROM join_table
                )
                SELECT gid, reachcode, 1 AS flag, (dp).path, (dp).geom
                FROM dump;
                END;""".format(self.schema)
        self.cursor.execute(SQL)
        
        if deleteOutsidePoints == True:
            # Delete endpoints from merged_points
            SQL =   """
                    BEGIN;
                    DELETE FROM {0}.merged_points mp
                    USING {0}.inner_2_points i2p
                    WHERE mp.gid = i2p.gid AND mp.geom && i2p.geom;
                    END;""".format(self.schema)
            self.cursor.execute(SQL)
            
        else:
            # Delete enpoints from temp_inside
            SQL =   """
                    BEGIN;
                    DELETE FROM {0}.temp_inside ti
                    USING {0}.inner_2_points i2p
                    WHERE ti.gid = i2p.gid AND ti.geom && i2p.geom;
                    END;""".format(self.schema)
            self.cursor.execute(SQL)
        
        # Append inner points to merged points
        SQL =   """
                BEGIN;
                INSERT INTO {0}.merged_points
                WITH temp_select AS
                (
                    SELECT *
                    FROM {0}.temp_inside
                    WHERE (gid = 3012 AND path[1] = 2) OR
                    (gid = 2922 AND path[1] = 2) OR
                    (gid = 769 AND path[1] = 2) OR 
                    (gid = 769 AND path[1] = 1) OR
                    (gid = 2949 AND path[1] = 2) OR
                    (gid = 2953 AND path[1] = 1) OR
                    (gid = 2134 AND path[1] = 1)
                )
                SELECT ti.*
                FROM {0}.temp_inside ti LEFT JOIN temp_select ts
                ON ti.gid = ts.gid AND ti.path = ts.path
                WHERE ts.gid IS NULL;
                END;""".format(self.schema)
        self.cursor.execute(SQL)
        
        # Order the points to be rebuilt to a line
        whenStatement = "gid != 3012 AND gid != 2922 AND gid != 769 AND gid != 2949 AND gid != 2953 AND gid != 2134"
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.ordered_points;
                CREATE TABLE {0}.ordered_points AS
                SELECT gid, reachcode, path, flag,
                CASE WHEN flag != 1 AND {1} AND array_length(path,1) = 2 AND path[1] = 2 THEN 10000 + path[2]
                WHEN flag = 1 AND array_length(path,1) = 1 THEN 1000 + path[1]
                WHEN flag = 1 AND gid = 3012 THEN 1500 + path[2]
                WHEN flag = 1 AND gid = 2922 THEN 1500 + path[2]
                WHEN flag = 1 AND gid = 769 THEN 3500 + path[2]
                WHEN flag = 1 AND gid = 2949 THEN 1500 + path[2]
                WHEN flag = 1 AND gid = 2953 THEN 2500 + path[2]
                WHEN flag = 1 AND gid = 2134 THEN 2500 + path[2]
                WHEN flag = 0 AND gid = 3012 THEN (path[1] * 1000) + path[2]
                WHEN flag = 0 AND gid = 2922 THEN (path[1] * 1000) + path[2]
                WHEN flag = 0 AND gid = 769 THEN (path[1] * 1000) + path[2]
                WHEN flag = 0 AND gid = 2949 THEN (path[1] * 1000) + path[2]
                WHEN flag = 0 AND gid = 2953 THEN (path[1] * 1000) + path[2]
                WHEN flag = 0 AND gid = 2134 THEN (path[1] * 1000) + path[2]
                ELSE path[2] END AS line_order, geom
                FROM {0}.merged_points
                ORDER BY gid, line_order;
                CREATE INDEX ordered_points_gist ON {0}.ordered_points USING GIST(geom);
                END;""".format(self.schema, whenStatement)
        self.cursor.execute(SQL)
        
    def extractToPoints(self, rasterDataset):
        """Extract raster data to points."""
        
        print "Extract to points from raster..."
        
        # Extract raster data to points    
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.raster_pts;
                CREATE TABLE {0}.raster_pts AS
                SELECT opts.gid, opts.reachcode, opts.line_order, opts.flag, ST_Value(myRast.rast, opts.geom) AS raster_val, opts.geom
                FROM {0}.ordered_points opts, {0}.{1} myRast
                WHERE ST_Intersects(rast, opts.geom);
                END;""".format(self.schema, rasterDataset)
        self.cursor.execute(SQL)
        
        # Create new elevations based on flag
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.raster_pts_height;
                CREATE TABLE {0}.raster_pts_height AS
                WITH first_join AS
                (    
                    SELECT a.gid, a.reachcode, a.line_order, a.flag, a.raster_val, b.height, a.geom
                    FROM {0}.raster_pts a LEFT JOIN {0}.inputs b ON a.reachcode = b.reach_code
                )
                SELECT gid, reachcode, line_order, 
                CASE WHEN flag = 1 THEN raster_val - height
                ELSE raster_val END AS raster_val, geom
                FROM first_join;
                END;""".format(self.schema)
        self.cursor.execute(SQL)   
        
        # Create 3d line from 3d points
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.lines_3d;
                CREATE TABLE {0}.lines_3d AS
                WITH ordered_points AS
                (
                    SELECT gid, line_order, raster_val, geom
                    FROM {0}.raster_pts_height
                    ORDER BY gid, line_order
                ),
                line3d AS
                (
                    SELECT gid, ST_MakeLine(ST_SetSRID(ST_MakePoint(ST_X(geom), ST_Y(geom), raster_val), 31983)) AS geom
                    FROM ordered_points
                    GROUP BY gid
                ) 
                SELECT xs.gid, xs.shape_leng AS shape_length, xs.hydroid AS joinid, l3d.geom
                FROM {0}.xs_cut_lines xs LEFT JOIN line3d l3d
                ON xs.gid = l3d.gid;
                END;""".format(self.schema)
        self.cursor.execute(SQL)   
      
    def cleanUpTables(self):
        """Delete all intermediate tables.""" 
        
        print "Cleaning up tables..."
        
        SQL =   """
                BEGIN;
                DROP TABLE IF EXISTS {0}.inner_2_points;
                DROP TABLE IF EXISTS {0}.merged_points;
                DROP TABLE IF EXISTS {0}.ordered_points;
                DROP TABLE IF EXISTS {0}.raster_pts;
                DROP TABLE IF EXISTS {0}.raster_pts_height;
                DROP TABLE IF EXISTS {0}.temp_inside;
                END;""".format(self.schema)
        self.cursor.execute(SQL) 
        
    def exportShapefile(self, projectionFile, outputShapefile, joinShapefile):     
        """Export shapefile using pgsql2shp.
        Need to do a join in Arc because PostGIS 
        is outputting integers as doubles for some reason."""
        
        print "Exporting shapefile..."
        
        tempShapefile = os.path.join(os.path.split(outputShapefile)[0], "temp.shp") 
        express = 'pgsql2shp -f "{0}" -k -h localhost -u postgres postgis "SELECT * FROM dem_smoothing.lines_3d"'.format(tempShapefile)
        os.chdir(r"C:\Program Files\PostgreSQL\9.2\bin")
        args = express
        p = subprocess.Popen(args)
        while p.poll() is None:
            time.sleep(1)
        finalProjection = outputShapefile[:-3] + "prj"
        
        arcpy.env.workspace = os.path.split(outputShapefile)[0]
        tempLayer = "tempLayer"
        arcpy.MakeFeatureLayer_management(tempShapefile, tempLayer)
        joinFields = ["XS2DID", "HydroID", "ProfileM", "RiverCode", "ReachCode", "LeftBank", 
                      "RightBank", "LLength", "ChLength", "RLength", "NodeName"]
        arcpy.JoinField_management(tempLayer, "joinid", joinShapefile, "XS2DID", joinFields)
        arcpy.CopyFeatures_management(tempLayer, outputShapefile)
        arcpy.DeleteField_management(outputShapefile, ["joinid"])
        shutil.copy(projectionFile, finalProjection)
        arcpy.Delete_management(tempShapefile)
        
def main():
    start = time.clock()
    
    csvTable = r"C:\Users\perryad\Desktop\Projects\2014_01_30_BurnRivers_Lorenzo\all_files\inputs4.csv"
    projectionFile = r"C:\Users\perryad\Desktop\Projects\2014_01_30_BurnRivers_Lorenzo\all_files\final3dLines.prj"
    outputShapefile = r"C:\Users\perryad\Desktop\Projects\2014_01_30_BurnRivers_Lorenzo\all_files\final3dLinesPostGIS.shp"
    joinShapefile = r"C:\Users\perryad\Desktop\Projects\2014_01_30_BurnRivers_Lorenzo\all_files\XSCutLines3D.shp"
    
    pgis = PostGIS(dbname="postgis", 
                   user="postgres", 
                   host="localhost", 
                   password="", 
                   schema="dem_smoothing")
    pgis.calculateHeight(csvTable)
    pgis.moveTable(csvTable)
    pgis.createLinesAndBuffers()
    pgis.createPoints(deleteOutsidePoints=False)
    pgis.extractToPoints(rasterDataset="nmap_dem_x2") # Use nmap or srtm to decide what gets extracted
    pgis.cleanUpTables()
    pgis.exportShapefile(projectionFile, outputShapefile, joinShapefile)
    
    end = time.clock()
    finalTime = end - start
    print "It took %s seconds" % finalTime
    
if __name__ == "__main__":
    main()
