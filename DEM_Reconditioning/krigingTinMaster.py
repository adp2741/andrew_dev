#-------------------------------------------------------------------------------
# Name:        DEMSmoothing.krigingTinMaster
# Purpose:
#
# Author:      Andrew Perry
#
# Created:     Jan 28, 2014
# Copyright:   (c) FM Global 2013
#-------------------------------------------------------------------------------
import pyWaveletNew
import arcview
import arcpy
from arcpy import env
from arcpy.sa import *
import os
import time

env.overwriteOutput = True

class CreateFolders(object):
    """Create folders and files to be used for the analysis."""
    
    def __init__(self, folderLoc, folderName, copyFileDict):
        """Instatiate object with all necessary files."""
        self.folderLoc = folderLoc
        self.folderName = folderName
        self.copyFileDict = copyFileDict
        # Folder paths
        self.shapefileFolder = "%s\\%s\\%s" % (self.folderLoc, self.folderName, "shapefiles")
        self.rasterFolder = "%s\\%s\\%s" % (self.folderLoc, self.folderName, "rasters")
        self.tableFolder = "%s\\%s\\%s" % (self.folderLoc, self.folderName, "tables")
        # New file locations
        self.demRaster = self.buildPath(self.rasterFolder, self.copyFileDict["demRaster"])
        self.pointFile = self.buildPath(self.shapefileFolder, self.copyFileDict["pointFile"])
        self.maskShape = self.buildPath(self.shapefileFolder, self.copyFileDict["maskShape"])
        self.rivOutline3d = self.buildPath(self.shapefileFolder, self.copyFileDict["rivOutline3d"])
        self.rivPolygon = self.buildPath(self.shapefileFolder, self.copyFileDict["rivPolygon"])
        
    def buildPath(self, folder, dictionaryPath):
        """Create paths for files to be used later. This is run while instantiating object."""
        return os.path.join(folder, os.path.split(dictionaryPath)[1])
        
    def makeFolders(self):
        """Create folder hierarchy."""
        os.mkdir("%s/%s" % (self.folderLoc, self.folderName))
        os.mkdir(self.shapefileFolder)
        os.mkdir(self.rasterFolder)
        os.mkdir(self.tableFolder)
        
    def copyFiles(self):
        """Copy files from old location to new folder hierarchy."""
        arcpy.CopyRaster_management(self.copyFileDict["demRaster"], self.demRaster)
        arcpy.CopyFeatures_management(self.copyFileDict["pointFile"], self.pointFile)
        arcpy.CopyFeatures_management(self.copyFileDict["maskShape"], self.maskShape)
        arcpy.CopyFeatures_management(self.copyFileDict["rivOutline3d"], self.rivOutline3d)
        arcpy.CopyFeatures_management(self.copyFileDict["rivPolygon"], self.rivPolygon)
       
    def getNewFolders(self):
        """Return all new folders to be used in ProcessShapefile class."""
        newFolderDict = {"shapefileFolder" : self.shapefileFolder,
                         "rasterFolder" : self.rasterFolder,
                         "tableFolder" : self.tableFolder
                         }
        return newFolderDict
        
    def getNewFiles(self):
        """Return all new file locations to be used in ProcessShapefile class."""
        newFileDict = {"demRaster" : self.demRaster,
                      "pointFile" : self.pointFile,
                      "maskShape" : self.maskShape,
                      "rivOutline3d" : self.rivOutline3d,
                      "rivPolygon" : self.rivPolygon
                      }
        return newFileDict
    

class ProcessData(object):
    """Run different analyses on raster and shapefiles."""
    
    def __init__(self, newFolderDict, newFileDict):
        """Instatiate object with all necessary files."""
        self.shapefileFolder = newFolderDict["shapefileFolder"]
        self.rasterFolder = newFolderDict["rasterFolder"]
        self.tableFolder = newFolderDict["tableFolder"].replace("\\", "/") + "/" #Need to do this for R
        
        self.demName = os.path.split(newFileDict["demRaster"])[1][:4]
        self.demRaster = newFileDict["demRaster"]
        self.pointFile = newFileDict["pointFile"]
        self.maskShape = newFileDict["maskShape"]
        self.rivOutline3d = newFileDict["rivOutline3d"]
        self.rivPolygon = newFileDict["rivPolygon"]
        self.correctedRaster = ""
        
    def runWavelet(self, raster):
        """Run wavelet on raster in question."""
        rasterName = os.path.split(raster)[1]
        if rasterName[-4:] == ".tif":
            outputName = "%s_%s" % (rasterName[:-4], "wav")
        else:
            outputName = "%s_%s" % (rasterName, "wav")
        finalRaster = os.path.join(self.rasterFolder, outputName)
        myWavelet = pyWaveletNew.Wavelet(raster, self.tableFolder, outputName)
        # If another raster other than nextmap is being used, the code below should be changed
        myWavelet.makeGroupOfTiles(sideLength=1024, yStart=0, xStart=250, yEnd=1972, xEnd=4346, 
                                   sideClip=20, levels=19, cellSize=30, topLeft=[281183.500292, 7424981.03917])
        myWavelet.mosaicCleanup()
        myWavelet.deleteTables()
        return finalRaster
    
    def mask(self, inputRaster):
        """Mask out outside area of raster."""
        if inputRaster[-4:] == ".tif":
            outputName = "%s_%s" % (inputRaster[:-4], "mask.tif")
        else:
            outputName = "%s_%s" % (inputRaster, "mask.tif")
        outExtractByMask = ExtractByMask(inputRaster, self.maskShape)
        outExtractByMask.save(outputName)
        return outputName
    
    def extractAndKrig(self):
        """Run extraction and kriging on processed raster."""
        wavRaster = self.runWavelet(self.demRaster) # Run wavelet
        env.extent = self.demRaster
        env.snapRaster = self.demRaster
        maskedRaster = self.mask(wavRaster) # Run mask
        
        print "\tExtracting to Points..."
        fieldList = [f.name for f in arcpy.ListFields(self.pointFile)]
        fieldName = "extractVal"
        if fieldName in fieldList:
            arcpy.DeleteField_management(self.pointFile, fieldName)
        inRasterList = [[maskedRaster, fieldName]]
        ExtractMultiValuesToPoints(self.pointFile, inRasterList)
        
        print "\tCalculating Fields..."
        fieldDifference = "final_diff"
        
        if fieldDifference in fieldList:
            arcpy.DeleteField_management(self.pointFile, fieldDifference)
        arcpy.AddField_management(self.pointFile, fieldDifference, "DOUBLE")
        arcpy.CalculateField_management(self.pointFile, fieldDifference, "!%s! - !%s!" % ("elev_m", fieldName), "PYTHON_9.3")
        
        print "\tKriging..."
        outKrigRast = os.path.join(self.rasterFolder, "krig_diff")
        outKrig = Kriging(self.pointFile, fieldDifference, KrigingModelOrdinary("GAUSSIAN", 30), 30, RadiusVariable(12))
        outKrig.save(outKrigRast)
        
        print "\tAdding rasters and then masking..."
        outPlus = Plus(maskedRaster, outKrigRast)
        self.correctedRaster = os.path.join(self.rasterFolder, "%s_%s" % (self.demName, "corr"))
        outPlus.save(self.correctedRaster)
        
    def createTIN(self):
        """Perform aggregation and construct TIN and raster."""
        arcpy.CheckOutExtension("3D")
        env.extent = ""
        env.snapRaster = ""
        wavRaster = self.runWavelet(self.correctedRaster) # Run wavelt
        env.extent = self.demRaster
        env.snapRaster = self.demRaster
        maskedRaster = self.mask(wavRaster)
        outAggRast = "%s_%s" % (self.correctedRaster, "150agg.tif")
        outAggreg = Aggregate(maskedRaster, 5, "MEAN")
        outAggreg.save(outAggRast)
        
        print "\tRaster to TIN..."
        outTin = os.path.join(self.rasterFolder, "150agg_z1")
        arcpy.RasterTin_3d(outAggRast, outTin, "1", "1500000", "1")
        
        print "\tTin to Raster..."
        outTinRaster = "%s_%s" % (self.correctedRaster, "150agg_z1.tif")
        arcpy.TinRaster_3d(outTin, outTinRaster, "FLOAT", "LINEAR", "CELLSIZE 150")
        
        print "\tRaster to Points..."
        outPointFile = os.path.join(self.shapefileFolder, "150agg_z1_points.shp")
        arcpy.RasterToPoint_conversion(outTinRaster, outPointFile, "VALUE")
    
        print "\tRemoving points within river..."
        env.workspace = os.path.split(outPointFile)[0]
        arcpy.MakeFeatureLayer_management(outPointFile, "temp_points")
        arcpy.SelectLayerByLocation_management("temp_points", "INTERSECT", self.rivPolygon)
        arcpy.SelectLayerByAttribute_management("temp_points", "SWITCH_SELECTION")
        
        newPointFile = os.path.join(self.shapefileFolder, "terrainPoints.shp")
        arcpy.CopyFeatures_management("temp_points", newPointFile)
        
        print "\tCreate TIN..."
        spatial_ref = arcpy.Describe(newPointFile).spatialReference
        in_features = "%s GRID_CODE Mass_Points <None>; %s Shape.Z Hard_Line <None>" % (newPointFile, self.rivOutline3d)
        outputTIN = os.path.join(self.rasterFolder, "final_z1")
        arcpy.CreateTin_3d(outputTIN, spatial_ref, in_features)
        
        print "\tTIN to Raster..."
        finalRaster = os.path.join(self.rasterFolder, "final_z1.tif")
        arcpy.TinRaster_3d(outputTIN, finalRaster, "FLOAT", "LINEAR", "CELLSIZE 10")
        
        print "\tMasking..."
        env.cellSize = 5
        finalMaskedRaster = os.path.join(self.rasterFolder, "%s_final.tif" % self.demName)
        outExtractByMask = ExtractByMask(finalRaster, self.maskShape)
        outExtractByMask.save(finalMaskedRaster)
        self.mask(finalRaster)
    
    
def main():
    start = time.clock()
    
    folderLoc = r"C:\Users\perryad\Desktop\Projects\2013_12_05_SaoPauloDEMProcessing"
    folderName = r"NMAP_NewData"
    origRaster = r"C:\Users\perryad\Desktop\Projects\2013_12_05_SaoPauloDEMProcessing\final_run_noLandsat\rasters\nmap_utm"
       
    copyFileDict =  {"demRaster" : origRaster,
                     "pointFile" : r"C:\Users\perryad\Desktop\Projects\2013_12_05_SaoPauloDEMProcessing\shapefiles\newHardLine_2014_02_014\100mCountour_150mDistance_WithOutlinePoints.shp",
                     "maskShape" : r"C:\Users\perryad\Desktop\Projects\2013_12_05_SaoPauloDEMProcessing\final_run_noLandsat\shapefiles\mask.shp",
                     "rivOutline3d" : r"C:\Users\perryad\Desktop\Projects\2013_12_05_SaoPauloDEMProcessing\shapefiles\newHardLine_2014_02_014\river_outline_3D.shp",
                     "rivPolygon" : r"C:\Users\perryad\Desktop\Projects\2013_12_05_SaoPauloDEMProcessing\shapefiles\newHardLine_2014_02_014\buffers_dissolve.shp"
                     }
    
    folderObj = CreateFolders(folderLoc, folderName, copyFileDict)
    folderObj.makeFolders()
    folderObj.copyFiles()
    newFolderDict = folderObj.getNewFolders()
    newFileDict = folderObj.getNewFiles()
    
    dataObj = ProcessData(newFolderDict, newFileDict)
    dataObj.extractAndKrig() # Needs to be run in order to use createTIN()
    dataObj.createTIN() # Needs extractAndKrig() ran
    
    end = time.clock()
    finalTime = end - start
    print "It took %s seconds" % finalTime
    
if __name__ == '__main__':
    main()
