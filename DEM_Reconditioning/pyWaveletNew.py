#-------------------------------------------------------------------------------
# Name:        scripts.pyWavelet
# Purpose:
#
# Author:      Andrew Perry
#
# Created:     Sep 23, 2013
# Copyright:   (c) FM Global 2013
#-------------------------------------------------------------------------------
import time
import os
import arcview
import arcpy
from arcpy import env
from arcpy.sa import *
import numpy as np
import rpy2.robjects as robjects
arcpy.CheckOutExtension("Spatial")


class Wavelet(object):
    """Class that creates a wavelet object that runs on specified grids of a raster to smooth."""
    def __init__(self, inputPath, outputTableDirectory, outputName):
        """Instantiate variables."""
        self.sideLength = 0
        self.sideClip = 0
        self.inputPath = inputPath
        self.myArray = arcpy.RasterToNumPyArray(inputPath,"","","",0)
        self.outputTableDirectory = outputTableDirectory.replace("\\", "/")
        self.outputRasterDirectory = os.path.split(self.inputPath)[0]
        self.outputName = outputName
        
        
    def deleteTables(self):
        """Delete all tables in table folder for new run."""
        for theFile in os.listdir(self.outputTableDirectory):
            filePath = os.path.join(self.outputTableDirectory, theFile)
            try:
                if os.path.isfile(filePath):
                    os.unlink(filePath)
            except Exception, e:
                print e
            
            
    def makeTile(self, yStart, xStart):
        """Create x*y grid to be smoothed in R."""
        self.myVector = []
        i = 0
        for x in self.myArray:
            if i >= yStart and i < (yStart + self.sideLength):
                j = 0
                for y in x:
                    if j >= xStart and j < (xStart + self.sideLength):
                        self.myVector.append(y)
                    j += 1     
            i += 1
        outputTable = os.path.join(self.outputTableDirectory, "%s_%s_%s.txt" % (self.sideLength, xStart, yStart))
        rOutputTable = "%s_fixed.txt" % outputTable[:-4]
        np.savetxt(outputTable, self.myVector, fmt='%0.8f')
        statement = """library('fields')
                       library('wavethresh');
                       x<-as.numeric(unlist(read.table("%s",header=FALSE)));
                       x2d<-matrix(x,%s,length(x)/%s);                         
                       wy<-wd(x2d, filter.number = 10, bc="periodic") #could be customized (e.g. change wavelet basis (mother wavelet)
                       thresh <- threshold(wy, levels = 0:%s, policy="manual", value = 4, type="soft") 
                       yr <- wr(thresh)
                       write.matrix(yr, file = "%s")
                       """ % (outputTable, self.sideLength, self.sideLength, self.level, rOutputTable)
                       
        robjects.r(statement) # Execute statement in R
        self.newVector = np.loadtxt(r"%s" % rOutputTable)
        self.newArray = np.reshape(self.newVector, (self.sideLength, self.sideLength))
        numpyOutput = self.newArray
        env.cellSize = self.inputPath
        env.outputCoordinateSyster = self.inputPath
        env.snapRaster = self.inputPath
        env.overwriteOutput = True
        newTopLeft = [0, 0]
        newTopLeft[0] = self.topLeft[0] + (self.cellSize * xStart)
        newTopLeft[1] = self.topLeft[1] - (self.cellSize * (yStart + self.sideLength))
        lowerPoint = arcpy.Point(newTopLeft[0], newTopLeft[1])
        myRaster = arcpy.NumPyArrayToRaster(numpyOutput, lowerPoint, self.cellSize, self.cellSize)
        myRaster.save(r"%s\%s_%s_%s" % (self.outputRasterDirectory, self.sideLength, xStart, yStart)) # add .tif if its breaking 
            
            
    def makeGroupOfTiles(self, sideLength, yStart, xStart, yEnd, xEnd, sideClip, levels, cellSize, topLeft): 
        """Parse out tiles to be sent to R to perform wavelet."""
        self.level = levels
        self.cellSize = cellSize
        self.sideClip = sideClip
        self.sideLength = sideLength
        self.topLeft = topLeft
        i = yStart
        while (i + sideLength) <= yEnd:
            j = xStart
            while (j + sideLength) <= xEnd:
                self.makeTile(i, j)
                j += (sideLength - (self.sideClip * 3))
            i += ((yEnd - sideLength) - (self.sideClip * 3))
            
            
    def mosaicCleanup(self):
        """Used for clipping rasters to remove boundary noise, mosaicing,
        and finally deleting all files except for the output."""
        env.workspace = self.outputRasterDirectory
        env.outputCoordinateSystem = self.inputPath
        listRasters = arcpy.ListRasters("%s*" % self.sideLength)
        for raster in listRasters:
            xMinResult = arcpy.GetRasterProperties_management(raster, "LEFT")
            xMin = xMinResult.getOutput(0)
            xMin = float(xMin) + (self.sideClip * self.cellSize)
            yMinResult = arcpy.GetRasterProperties_management(raster, "BOTTOM")
            yMin = yMinResult.getOutput(0)
            yMin = float(yMin) + (self.sideClip * self.cellSize)
            xMaxResult = arcpy.GetRasterProperties_management(raster, "RIGHT")
            xMax = xMaxResult.getOutput(0)
            xMax = float(xMax) - (self.sideClip * self.cellSize)
            yMaxResult = arcpy.GetRasterProperties_management(raster, "TOP")
            yMax = yMaxResult.getOutput(0)
            yMax = float(yMax) - (self.sideClip * self.cellSize)
            inRectangle = Extent(xMin, yMin, xMax, yMax)
            rectExtract = ExtractByRectangle(raster, inRectangle, "INSIDE")
            newRaster = "%s_a.tif" % raster[:-4]
            rectExtract.save(newRaster)
            arcpy.Delete_management(raster)
        listRasters = arcpy.ListRasters("%s*" % self.sideLength)
        rasterString = ";".join(listRasters)
        arcpy.MosaicToNewRaster_management(rasterString, self.outputRasterDirectory, self.outputName, "", "", "%s" % self.cellSize, "1", "MEAN","FIRST")
        for raster in listRasters:
            arcpy.Delete_management(raster)


def main():
    start = time.clock()
    rasterPath = r"C:\Users\perryad\Desktop\Projects\2013_12_05_SaoPauloDEMProcessing\TestFolder\rasters2\srtm30m"
    outputName = "srtm_wav_2"
    outputTableDirectory = r"C:/Users/perryad/Desktop/Projects/2013_12_05_SaoPauloDEMProcessing/TestFolder/tables/"
    myWavelet = Wavelet(rasterPath, outputTableDirectory, outputName) # For cgiar
    myWavelet.deleteTables()
    myWavelet.makeGroupOfTiles(sideLength=1024, yStart=0, xStart=250, yEnd=1972, xEnd=4346, 
                               sideClip=20, levels=19, cellSize=30, topLeft=[281183.500292, 7424981.03917]) # For nextmap landsat
    myWavelet.mosaicCleanup()
    end = time.clock()
    finalTime = end - start
    print "It took %s seconds" % finalTime
    
    
if __name__ == '__main__':
    main()
