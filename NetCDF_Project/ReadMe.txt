The following C and Python programs were used to take the large amount of Grib files found on NOAA's server and centralize them into one location. The data collected from NOAA is to be used for 30 years worth of hourly data.

GetGrib.py - Retrieves grib data via FTP from NOAA
WriteNETCDF_Batch.py - Convert grib data to netcdf and then concatenate to yearly and then for 30 years
WritePrateBatch.py - Same as above but specific to precipitation rate
Read31YearFileWeights.c - Reads the 30 year NetCDF file of choice (precipitation, humidity, wind or temperature) as well as the weight file (what percent of each location (x,y) of the netcdf grib will be used to sum up for each HRR location (catchments zones). Returns the 30 year hourly data for each HRR location in the form of a binary file.
ReadNetCDFSubset.c - Reads from the binary file created from the above program and takes a specific HRR ID that the user wants to get hourly data for and returns it in a text file.

Just a note, the data we are using is very large. Each netcdf file is ~700gb in size because it holds x(1152) * y(576) * hour(30 * 8760). It is important that we break the data up into subsections to extract HRR zones. That is why the grid was created. These grids are just large enough to get enough HRR zones without running out of memory.