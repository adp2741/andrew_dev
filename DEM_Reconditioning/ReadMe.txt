This project was performed in order to smooth the elevation data. Currently there is a lot of noise in the data resulting in spikes from one pixel to the next.
The scripts below were used for the following:
pyWaveletNew.py - Script that accesses R Statistics to perform 2d wavelet on the image data it receives.
krigingTinMaster.py - The full program that takes a raster and uses the pyWaveletNew.py script and then continues to recondition the data using krigin and aggregation.
Create3dLinesPostGIS.py - This script takes the cross sections created by the C# application and creates 3d lines from them using Postgresql's spatial databse capabilities (PostGIS). The resulting 3d lines were then used in HEC-RAS.