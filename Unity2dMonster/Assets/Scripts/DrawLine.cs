﻿using UnityEngine;
using System.Collections;

public class DrawLine : MonoBehaviour 
{
	private LineRenderer lineRenderer;
	private float counter = 0f;
	private float dist;
	private Vector3 newOrigin;
	private Vector3 newDestination;

	public Transform origin;
	public Transform destination;
	public float lineDrawSpeed = 0.025f;

	// Use this for initialization
	void Start() 
	{
		lineRenderer = GetComponent<LineRenderer>();
		newOrigin = origin.position;
		newOrigin.z = -5;
		newDestination = destination.position;
		newDestination.z = -5;
		lineRenderer.SetPosition(0, newOrigin);
		lineRenderer.SetWidth(0.25f, 0.25f);
		lineRenderer.sortingOrder = -1;

		dist = Vector3.Distance(origin.position, destination.position);
		Debug.Log(dist);
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (counter < dist)
		{
			Debug.Log("Alan!");
			counter += 0.1f * lineDrawSpeed;

			float x = Mathf.Lerp(0, dist, counter);
			//float x = Time.deltaTime * lineDrawSpeed.

			Vector3 pointA = newOrigin;
			Vector3 pointB = newDestination;

			Vector3 pointAlongLine = x * Vector3.Normalize(pointB - pointA) + pointA;

			lineRenderer.SetPosition(1, pointAlongLine);
		}
	}
}
