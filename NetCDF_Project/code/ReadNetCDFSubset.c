/**
Reads data from binary by specifying HRR Id and write to binary/text.

Created by Andrew Perry
FM Global
**/

#include <stdint.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "C:/Users/perryad/Desktop/Projects/netCDF 4.3.1.1/include/netcdf.h"

// Read readme file to get HRR id position and length
void getBinaryPosition(char* readMe, int hrrId, int* lineNumber, int* hourNumber, int* lengthOfArray)
{
	FILE *readMeFile = fopen(readMe, "r");
	char readerLine[200];
	fgets(readerLine, sizeof readerLine, readMeFile);

	int lineMax;
	int numHours;
	sscanf(readerLine, "%d %d", &lineMax, &numHours);
	*lengthOfArray = numHours;

	int lineIter;
	for (lineIter = 0; lineIter < lineMax; lineIter++)
	{
        char currentLine[200];
		fgets(currentLine, sizeof currentLine, readMeFile);
		int currentHRR;
		sscanf(currentLine, "%d", &currentHRR);
		if (hrrId == currentHRR)
        {
            printf("Found in position: %d\n", lineIter);
            *lineNumber = lineIter;
            *hourNumber = numHours;
        }
	}
	fclose(readMeFile);
}

// Read binary using readme files information
uint8_t* readBinary(char* binaryFile, int lineNumber, int hourNumber, int lengthOfArray)
{
    int position = lineNumber * hourNumber * sizeof(uint8_t);
    printf("position: %d\tlength of array: %d\n", position, lengthOfArray);
    FILE *binaryOpen = fopen(binaryFile, "rb");
    uint8_t *hrrArray = malloc(sizeof(uint8_t) * lengthOfArray);
    fseek(binaryOpen, position, SEEK_SET);
    fread(hrrArray, sizeof(uint8_t), lengthOfArray, binaryOpen);
    fclose(binaryOpen);
    return hrrArray;
}

// Write HRR array to new file
void writeBinary(char* outputFile, uint8_t* hrrArray, int lengthOfArray)
{
    FILE *writeOpen = fopen(outputFile, "w");
    int iter;
    for (iter = 0; iter < lengthOfArray; iter++)
    {
        fprintf(writeOpen, "%d\n", hrrArray[iter]);
    }
    fclose(writeOpen);
    free(hrrArray);
}

int main(int argc, char **argv)
{
    clock_t begin, end;
    double time_spent;
    begin = clock();

    if (argc < 5)
    {
        printf("You did not give enough arguments.\nPlease give -readMe -binaryFile -outputFile -hrrId.\n");
    }
    char *readMe = argv[1];
    char *binaryFile = argv[2];
    char *outputFile = argv[3];
    int hrrId;
    sscanf(argv[4], "%d", &hrrId);
    int lineNumber;
    int hourNumber;
    int lengthOfArray;
    getBinaryPosition(readMe, hrrId, &lineNumber, &hourNumber, &lengthOfArray);
    float *hrrArray = readBinary(binaryFile, lineNumber, hourNumber, lengthOfArray);
    writeBinary(outputFile, hrrArray, lengthOfArray);
    //readWriteBinaryTest(binaryFile);

    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("It took %f seconds...\n", time_spent);
    return 1;
}
