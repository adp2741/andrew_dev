﻿using UnityEngine;

public class GUIManager : MonoBehaviour 
{
	
	public GUIText bestRunText, boostsText, distanceText, gameOverText, instructionsText, runnerText;
	public static GUIManager instance;
	private float bestDistance = 0f;

	void Start() 
	{
		instance = this;
		GameEventManager.GameStart += GameStart;
		GameEventManager.GameOver += GameOver;
		gameOverText.enabled = false;
	}
	
	void Update() 
	{
		if (Input.GetButtonDown("Jump") || (Input.GetMouseButtonDown(0)))
		{
			GameEventManager.TriggerGameStart();
		}
	}

	public static void SetBoosts(int boosts)
	{
		instance.boostsText.text = boosts.ToString();
	}

	public static void SetDistance(float distance)
	{
		instance.distanceText.text = distance.ToString("0.0");
	}

	private void GameStart() 
	{
		gameOverText.enabled = false;
		instructionsText.enabled = false;
		runnerText.enabled = false;
		enabled = false;
	}

	private void GameOver() 
	{
		gameOverText.enabled = true;
		instructionsText.enabled = true;
		if (Runner.distanceTraveled > bestDistance)
		{
			bestRunText.text = string.Format("Best: {0:0.0}", Runner.distanceTraveled);
			bestDistance = Runner.distanceTraveled;
		}
		enabled = true;
	}
}
