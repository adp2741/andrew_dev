﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	static int playerScore01 = 0;
	static int playerScore02 = 0;

	public GUISkin theSkin;
	public Transform theBall;

	void Start()
	{
		theBall = GameObject.FindGameObjectsWithTag("Ball")[0].transform;
	}

	// Update is called once per frame
	public static void Score(string wallName) 
	{
		if (wallName == "rightWall") 
		{
			playerScore01 += 1;
		}
		else if (wallName == "leftWall") 
		{
			playerScore02 += 1;
		}
	}

	void OnGUI()
	{
		GUI.skin = theSkin;
		GUI.Label(new Rect(Screen.width / 2 - 350, 20, 300, 100), string.Format("Player 1:\n{0}", playerScore01));
		GUI.Label(new Rect(Screen.width / 2 + 50, 20, 300, 100), string.Format("Player 2:\n{0}", playerScore02));
		if (GUI.Button(new Rect(Screen.width/2 - 121/2, 35, 131, 53), "RESET"))
		{
			playerScore01 = 0;
			playerScore02 = 0;

			theBall.gameObject.SendMessage("ResetBall");
		}
	}
}
