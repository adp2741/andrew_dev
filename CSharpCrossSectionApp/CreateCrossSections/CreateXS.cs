﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.DataSourcesGDB;
using ESRI.ArcGIS.DataSourcesFile;
using ESRI.ArcGIS.Geoprocessing;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.ArcMap;

namespace CreateCrossSections
{
    // Class that contains methods for creating cross sections and points
    class CreateXS
    {
        public string inputShapefile = null;
        public string outputPointShapefile = null;
        public string outputCrossShapefile = null;
        public double interval = 0.0;
        public long minimumTicks = 0;

        // Create feature class from input shapefile and get the spatial reference
        public IFeatureClass OpenFromFile(string strFileName, ref ISpatialReference spatialReference, ref IFeatureWorkspace myFWS)
        {
            string pathname = System.IO.Path.GetDirectoryName(strFileName);
            string filename = System.IO.Path.GetFileName(strFileName);
            string strFileExtension = null;

            myFWS = OpenFeatureWorkspace(pathname, ref strFileExtension);
            spatialReference = null;
            IFeatureClass tempFeatureClass = myFWS.OpenFeatureClass(filename);
            int tempShapeFieldpos = tempFeatureClass.FindField(tempFeatureClass.ShapeFieldName);
            IFields tempFields = tempFeatureClass.Fields;
            IField tempField = tempFields.get_Field(tempShapeFieldpos);
            spatialReference = tempField.GeometryDef.SpatialReference;
            return tempFeatureClass;
        }

        // Create empty output files based on type given
        public IFeatureClass CreateOutputFile(string strFileName, ISpatialReference spatialReference, string type_id)
        {
            string strWorkspace = System.IO.Path.GetDirectoryName(strFileName);
            string strFileExtension = System.IO.Path.GetExtension(strFileName);
            string strFileOutputName = System.IO.Path.GetFileName(strFileName);

            IFeatureWorkspace featureWorkspace = OpenFeatureWorkspace(strWorkspace, ref strFileExtension);
            IGeometryDef geometryDefShape = new GeometryDefClass();
            IGeometryDefEdit geometryDefEditShape = (IGeometryDefEdit)geometryDefShape;
            if (type_id.ToLower() == "point")
            {
                geometryDefEditShape.GeometryType_2 = esriGeometryType.esriGeometryPoint;
                geometryDefEditShape.HasZ_2 = false;
                geometryDefEditShape.HasM_2 = false;
                geometryDefEditShape.SpatialReference_2 = spatialReference;
            }
            else if (type_id.ToLower() == "line")
            {
                geometryDefEditShape.GeometryType_2 = esriGeometryType.esriGeometryPolyline;
                geometryDefEditShape.SpatialReference_2 = spatialReference;
            }
            else if (type_id.ToLower() == "polygon")
            {
                geometryDefEditShape.GeometryType_2 = esriGeometryType.esriGeometryPolygon;
                geometryDefEditShape.SpatialReference_2 = spatialReference;
            }

            IFields fields = new FieldsClass();
            IFieldsEdit fieldsEdit = (IFieldsEdit)fields;

            // Add an ObjectID field to the fields collection. This is mandatory for feature classes.
            IField oidField = new FieldClass();
            IFieldEdit oidFieldEdit = (IFieldEdit)oidField;
            oidFieldEdit.Name_2 = "OID";
            oidFieldEdit.Type_2 = esriFieldType.esriFieldTypeOID;
            fieldsEdit.AddField(oidField);

            IField fieldRivID = new FieldClass();
            IFieldEdit fieldRivIDEdit = (IFieldEdit)fieldRivID;
            fieldRivIDEdit.Name_2 = "RivID";
            fieldRivIDEdit.Type_2 = esriFieldType.esriFieldTypeInteger;
            fieldsEdit.AddField(fieldRivID);

            IField fieldRivName = new FieldClass();
            IFieldEdit fieldRivNameEdit = (IFieldEdit)fieldRivName;
            fieldRivNameEdit.Name_2 = "RivName";
            fieldRivNameEdit.Type_2 = esriFieldType.esriFieldTypeString;
            fieldRivNameEdit.Length_2 = 20;
            fieldsEdit.AddField(fieldRivName);

            IField fieldXSID = new FieldClass();
            IFieldEdit fieldXSIDEdit = (IFieldEdit)fieldXSID;
            fieldXSIDEdit.Name_2 = "XSID";
            fieldXSIDEdit.Type_2 = esriFieldType.esriFieldTypeInteger;
            fieldsEdit.AddField(fieldXSID);

            IField fieldDistance = new FieldClass();
            IFieldEdit fieldDistanceEdit = (IFieldEdit)fieldDistance;
            fieldDistanceEdit.Name_2 = "Distance";
            fieldDistanceEdit.Type_2 = esriFieldType.esriFieldTypeDouble;
            fieldsEdit.AddField(fieldDistance);

            IField geometryField = new FieldClass();
            IFieldEdit geometryFieldEdit = (IFieldEdit)geometryField;
            geometryFieldEdit.Name_2 = "Shape";
            geometryFieldEdit.Type_2 = esriFieldType.esriFieldTypeGeometry;
            geometryFieldEdit.GeometryDef_2 = geometryDefShape;
            fieldsEdit.AddField(geometryField);

            // Use IFieldChecker to create a validated fields collection.
            IFieldChecker fieldChecker = new FieldCheckerClass();
            IEnumFieldError enumFieldError = null;
            IFields validatedFields = null;
            fieldChecker.ValidateWorkspace = (IWorkspace)featureWorkspace;
            fieldChecker.Validate(fields, out enumFieldError, out validatedFields);

            // The enumFieldError enumerator can be inspected at this point to determine 
            // which fields were modified during validation.

            if (CleanFeatureClass((IWorkspace)featureWorkspace, strFileOutputName) > 0)
            {
                System.Console.WriteLine(String.Format("Cannot Overwrite Output File {0}.  Quitting.", strFileOutputName));
                return null;
            }
            IFeatureClass fclassOutput = featureWorkspace.CreateFeatureClass(strFileOutputName, validatedFields, null, null, esriFeatureType.esriFTSimple, "Shape", "");

            return fclassOutput;
        }

        // Figure out what type of workspace it is /path/to/shapefile vs. /path/to.gdb/featureclass
        private IFeatureWorkspace OpenFeatureWorkspace(string strFolder, ref string strFileExtension)
        {
            IWorkspaceFactory2 workspaceFactory = null;
            if (strFolder.Contains(".gdb"))
            {
                workspaceFactory = new FileGDBWorkspaceFactoryClass();
                strFileExtension = "";
            }
            else if (strFolder.Contains(".mdb"))
            {
                workspaceFactory = new AccessWorkspaceFactoryClass();
                strFileExtension = "";
            }
            else
            {
                workspaceFactory = new ShapefileWorkspaceFactoryClass();
                strFileExtension = ".shp";
            }
            return (IFeatureWorkspace)workspaceFactory.OpenFromFile(strFolder, 0);
        }

        // Clean feature classes by removing all datasets associated with old shapefile
        public static int CleanFeatureClass(IWorkspace workspace, string file)
        {
            IDataset dataset;
            string filePath = String.Format("{0}\\{1}", workspace.PathName, file);
            int i = 0;
            while (File.Exists(filePath))
            {
                i++;
                if (i > 10)
                {
                    return 1;
                }
                IFeatureWorkspace featureWorkspace = (IFeatureWorkspace)workspace;

                dataset = (IDataset)featureWorkspace.OpenFeatureClass(file);

                if (dataset.CanDelete())
                {
                    dataset.Delete();
                }
            }
            return 0;
        }

        // Create cross sections along line
        public bool CreateCrossSection(IFeatureClass myPolylineFC, IFeatureClass myPointFC, IFeatureClass myNormalFC, double interval, double tickWidth, long minimumTicks)
        {
            try
            {
                System.Console.WriteLine("Processing polylines ... ");

                // Set up points
                IFeatureBuffer fbufferOutput = myPointFC.CreateFeatureBuffer();
                IFeatureCursor fcursorPts = myPointFC.Insert(false);
                IFields ptsfields = fcursorPts.Fields;
                int iPtsRivIDpos = myPointFC.FindField("RivID");
                int iPtsRivNamepos = myPointFC.FindField("RivName");
                int iPtsXSIDpos = myPointFC.FindField("XSID");
                int iPtsDistance = myPointFC.FindField("Distance");

                // Set up normal lines for cross section
                IFeatureBuffer fbufferOutputNormal = myNormalFC.CreateFeatureBuffer();
                IFeatureCursor fcursorPtsNormal = myNormalFC.Insert(false);
                IFields ptsfieldsNormal = fcursorPtsNormal.Fields;
                int iPtsRivIDposNormal = myNormalFC.FindField("RivID");
                int iPtsRivNameposNormal = myNormalFC.FindField("RivName");
                int iPtsXSIDposNormal = myNormalFC.FindField("XSID");

                // Set up input file
                IFeatureCursor fcursorLines = myPolylineFC.Search(null, false);
                IFeature featureLine = fcursorLines.NextFeature();
                IFields linefields = myPolylineFC.Fields;

                IGeometry outGeometry = new PolylineClass();
                IGeoDataset geoDataset = myNormalFC as IGeoDataset;
                outGeometry.SpatialReference = geoDataset.SpatialReference;
                IGeometryCollection geomCollect = outGeometry as IGeometryCollection;
                IGeometryCollection geomCollect2 = outGeometry as IGeometryCollection;
                IPoint tempPts = new PointClass();
                ILine normal = new LineClass();
                ILine normalOpp = new LineClass();
                IPoint lineStartPts = new PointClass();

                int tempLID = 0;
                ICurve tempCurve;

                double normalLength = tickWidth / 2;
                double oppositeLength = 0 - normalLength; // Create lines on both side of point

                while (featureLine != null)
                {
                    tempCurve = (ICurve)featureLine.Shape;
                    IPolyline pLine = null;
                    pLine = featureLine.Shape as IPolyline;
                    double segmentLength = pLine.Length; // Get length of current input line that is going to be split
                    double[] alongLine;

                    // If segment is small and we still want points / xs along line, do the following:
                    if ((interval * minimumTicks) > segmentLength)
                    {
                        alongLine = new double[minimumTicks + 2]; // Add 2 to account for end points
                        for (int x = 0; x < alongLine.Length; x++)
                        {
                            if (x == 0)
                            {
                                alongLine[x] = 0.0; // Comment out if not endpoint
                            }
                            else if (x == alongLine.Length - 1)
                            {
                                alongLine[x] = segmentLength; // Comment out if not endpoint
                            }
                            else
                            {
                            alongLine[x] = (x) * (segmentLength / (minimumTicks + 1));
                            }
                        }
                    }
                    // Otherwise we go through the segment at each interval
                    else
                    {
                        double arraySize = Math.Floor(segmentLength / interval);
                        alongLine = new double[Convert.ToInt16(arraySize) + 2]; // Add 2 to account for end points
                        for (int x = 0; x < alongLine.Length; x++)
                        {
                            if (x == 0)
                            {
                                alongLine[x] = 0.0; // Comment out if not endpoint
                            }
                            else if (x == alongLine.Length - 1)
                            {
                                alongLine[x] = segmentLength; // Comment out if not endpoint
                            }
                            else
                            {
                                alongLine[x] = x * interval;
                            }
                        }
                    }
                    // Loop through array of all distances for points and cross sections
                    for (int i = 0; i < alongLine.Length; i++) // (= 1) (-1) added
                    {
                        try
                        {
                            // Set distance field for each point / line.
                            // If interval is 100, all points will have 100 as distance except
                            // for beginning point and endpoint which will be 0 and between 0 and 100 respectively
                            if (i == 0)
                            {
                                fbufferOutput.set_Value(iPtsDistance, 0);
                            }
                            else if (i == alongLine.Length - 1)
                            {
                                double distance = segmentLength - ((alongLine.Length - 2) * interval);
                                fbufferOutput.set_Value(iPtsDistance, distance);
                            }
                            else
                            {
                                fbufferOutput.set_Value(iPtsDistance, interval);
                            }
                            // Populate points in shapefile
                            tempCurve.QueryPoint(esriSegmentExtension.esriNoExtension, Convert.ToDouble(alongLine[i]), false, tempPts);
                            fbufferOutput.Shape = (IGeometry)tempPts;
                            fbufferOutput.set_Value(iPtsRivIDpos, tempLID);
                            fbufferOutput.set_Value(iPtsXSIDpos, i);
                            fcursorPts.InsertFeature(fbufferOutput);
                            fcursorPts.Flush();

                            // Populate first side of normal lines
                            tempCurve.QueryNormal(esriSegmentExtension.esriNoExtension, Convert.ToDouble(alongLine[i]), false, normalLength, normal);
                            ISegmentCollection newPath = new PathClass();
                            object missing = Type.Missing;
                            newPath.AddSegment(normal as ISegment, ref missing, ref missing);
                            geomCollect.AddGeometry(newPath as IGeometry, ref missing, ref missing);
                            IPolyline newPolyline = geomCollect as IPolyline;
                            fbufferOutputNormal.Shape = (IGeometry)newPolyline;
                            fbufferOutputNormal.set_Value(iPtsRivIDpos, tempLID);
                            fbufferOutputNormal.set_Value(iPtsXSIDpos, i);
                            fcursorPtsNormal.InsertFeature(fbufferOutputNormal);
                            fcursorPtsNormal.Flush();

                            // Populate opposite side of normal lines
                            tempCurve.QueryNormal(esriSegmentExtension.esriNoExtension, Convert.ToDouble(alongLine[i]), false, oppositeLength, normalOpp);
                            ISegmentCollection newPath2 = new PathClass();
                            object missing2 = Type.Missing;
                            newPath2.AddSegment(normalOpp as ISegment, ref missing2, ref missing2);
                            geomCollect2.AddGeometry(newPath2 as IGeometry, ref missing2, ref missing2);
                            IPolyline newPolyline2 = geomCollect2 as IPolyline;
                            fbufferOutputNormal.Shape = (IGeometry)newPolyline2;
                            fbufferOutputNormal.set_Value(iPtsRivIDpos, tempLID);
                            fbufferOutputNormal.set_Value(iPtsXSIDpos, i);
                            fcursorPtsNormal.InsertFeature(fbufferOutputNormal);
                            fcursorPtsNormal.Flush();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("{0} Exception caught.", e);
                        }
                    }
                    System.Console.Write(" {0} points added.\n", alongLine.Length - 1);
                    featureLine = fcursorLines.NextFeature();
                    tempLID = tempLID + 1;
                }
                // Release locks on dataset. Currently not working 100%
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(fcursorPts);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(fcursorLines);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(fbufferOutput);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error With Cross Section");
                Console.WriteLine(String.Format("{0}", e.Message));
                return false;
            }
            return true;
        }
    }
}
