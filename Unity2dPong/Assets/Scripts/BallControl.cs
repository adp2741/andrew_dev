﻿using UnityEngine;
using System.Collections;

public class BallControl : MonoBehaviour 
{
	private float newVelocity = 0f;
	public float ballSpeed = 100;

	// Use this for initialization
	IEnumerator Start() 
	{
		yield return new WaitForSeconds(2);
		GoBall();
	}

	void Update()
	{
		float xVel = rigidbody2D.velocity.x;
		if (xVel < 18 && xVel > -18 && xVel != 0)
		{
			if (xVel > 0)
			{
				rigidbody2D.velocity = new Vector2(20, rigidbody2D.velocity.y);
			}
			else
			{
				rigidbody2D.velocity = new Vector2(-20, rigidbody2D.velocity.y);
			}
			Debug.Log(string.Format("Velocity before: {0}", xVel));
			Debug.Log(string.Format("Velocity after: {0}", rigidbody2D.velocity.x));
		}
	}
	
	void OnCollisionEnter2D(Collision2D colInfo)
	{
		if (colInfo.collider.tag == "Player")
		{
			newVelocity = rigidbody2D.velocity.y;
			newVelocity = newVelocity / 2 + colInfo.collider.rigidbody2D.velocity.y / 3;
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, newVelocity);
			audio.pitch = Random.Range(0.8f, 1.2f);
			audio.Play();
		}
	}

	IEnumerator ResetBall()
	{
		rigidbody2D.velocity = new Vector2(0, 0);
		rigidbody2D.transform.position = new Vector3(0, 0, 0);

		yield return new WaitForSeconds(1);
		GoBall();
	}

	void GoBall()
	{
		int randomNumber = Random.Range(0, 10);
		if (randomNumber <= 5) 
		{
			rigidbody2D.AddForce(new Vector2(ballSpeed, -5));
		}
		else
		{
			rigidbody2D.AddForce(new Vector2(-1 * ballSpeed, -5));
		} 
	}
}
