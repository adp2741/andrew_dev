using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.DataSourcesGDB;
using ESRI.ArcGIS.DataSourcesFile;
using ESRI.ArcGIS.Geoprocessing;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.ArcMap;

namespace CreateCrossSections
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // Browse for input shapefile
        private void button1_Click_1(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName))
                {
                    textBox1.Text = System.IO.Path.GetFullPath(openFileDialog1.FileName);
                }
            }
        }

        // Browse for output point location
        private void button3_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox2.Text = System.IO.Path.GetFullPath(saveFileDialog1.FileName);
            }
        }

        // Browse for output cross section location
        private void button4_Click(object sender, EventArgs e)
        {
            if (saveFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox4.Text = System.IO.Path.GetFullPath(saveFileDialog2.FileName);
            }
        }

        // Run the program with this button
        private void button2_Click(object sender, EventArgs e)
        {
            // Handle null values
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "")
            {
                label7.Text = "Enter Values!";
            }
            else
            {
                ISpatialReference InputSpatialReference = new UnknownCoordinateSystemClass();
                IFeatureWorkspace myFWS = null;
                string inputShapefile = textBox1.Text;
                string outputShapefile = textBox2.Text;
                string outputCrossShapefile = textBox4.Text;
                double interval = Convert.ToDouble(textBox3.Text);
                double tickWidth = Convert.ToDouble(textBox5.Text);
                long minTicks = Convert.ToInt64(textBox6.Text);
                CreateXS shapefileProcessor = new CreateXS();
                IFeatureClass inputPolygon = shapefileProcessor.OpenFromFile(inputShapefile, ref InputSpatialReference, ref myFWS);
                string lineID = "point";
                IFeatureClass outputPoint = shapefileProcessor.CreateOutputFile(outputShapefile, InputSpatialReference, lineID);
                string lineID2 = "line";
                IFeatureClass outputLine = shapefileProcessor.CreateOutputFile(outputCrossShapefile, InputSpatialReference, lineID2);
                label7.Text = "Working...";
                label7.Update();
                shapefileProcessor.CreateCrossSection(inputPolygon, outputPoint, outputLine, interval, tickWidth, minTicks);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(myFWS);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(inputPolygon);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(outputPoint);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(outputLine);
                label7.Text = "Done!";
                label7.Update();
            }
        }
    }
}