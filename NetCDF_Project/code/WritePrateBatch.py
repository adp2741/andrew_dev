#-------------------------------------------------------------------------------
# Name:        GribDownload.WritePrateBatch
# Purpose:
#
# Author:      Andrew Perry
#
# Created:     May 20, 2014
# Copyright:   (c) FM Global 2014
#-------------------------------------------------------------------------------
import os
import sys
import subprocess
import time

class WritePrateBatch(object):
    """A class that contains tools for working with PRATE data."""
    
    def __init__(self, gribFolder, hourlyFolder, codeFolder, netcdfFolder):
        """Instantiate the object."""
        self.gribFolder = gribFolder
        self.hourlyFolder = hourlyFolder
        self.codeFolder = codeFolder
        self.netcdfFolder = netcdfFolder
    
    def createHourly(self):
        """Open up 3 processes and then get hourly precipitation from 
        6 hour averages using -ncep_norm."""
        start = time.clock()
        for year in xrange(1979, 2010, 1):
            for month in xrange(1, 13, 3):
                yearMonth1 = "{0}{1:02}".format(year, month)
                yearMonth2 = "{0}{1:02}".format(year, month+1)
                yearMonth3 = "{0}{1:02}".format(year, month+2)
                pids = []
                myFilesToNetCDF = []
                for myFile in os.listdir(self.gribFolder):
                    if (yearMonth1 in myFile or yearMonth2 in myFile or yearMonth3 in myFile):
                        outputmyFile = "{0}Hourly{1}".format(myFile[:-5],myFile[-5:])
                        myFilesToNetCDF.append(outputmyFile)
                        makeArgument = "wgrib2 ../gribs/{0} \
                        -ncep_norm ../hourly_gribs/{1}".format(myFile, outputmyFile)
                        os.chdir(self.codeFolder)
                        FNULL = open(os.devnull, 'w')
                        p = subprocess.Popen(makeArgument, stdout=FNULL, stderr=subprocess.STDOUT)
                        pids.append(p)
                for p in pids:
                    p.wait()
                pids = []
                for myFile2 in myFilesToNetCDF:
                    makeArgument2 = "wgrib2 ../hourly_gribs/{0} \
                    -netcdf ../netcdf/{1}.nc".format(myFile2, myFile2[:-5])
                    FNULL = open(os.devnull, 'w')
                    p = subprocess.Popen(makeArgument2, stdout=FNULL, stderr=subprocess.STDOUT)
                    pids.append(p)
                for p in pids:
                    p.wait()
                print "FINISHED {0} {1} {2}".format(yearMonth1, yearMonth2, yearMonth3)
                print "It took {0} seconds".format(time.clock() - start)
                
    def createYearly(self, varType, year):
        """Create yearly netcdf myFiles by appending them together.
        varType can be q2m, tmp2m, prate or wnd10m."""
        yearlyList = []
        myFilesToDelete = []
        for month in xrange(1, 13, 1):
            yearMonth = "{0}{1:02}".format(year, month)
            for myFile in os.listdir(self.netcdfFolder):
                if varType in myFile and yearMonth in myFile:
                    print myFile
                    yearlyList.append(myFile)
                    myFilesToDelete.append(os.path.join(self.netcdfFolder, myFile))
        makeArgument = "ncrcat {0} {1} {2} {3} {4} \
        {5} {6} {7} {8} {9} {10} {11} {12}".format(yearlyList[0], yearlyList[1], yearlyList[2],
                                                    yearlyList[3], yearlyList[4], yearlyList[5],
                                                    yearlyList[6], yearlyList[7], yearlyList[8],
                                                    yearlyList[9], yearlyList[10], yearlyList[11],
                                                    "../hourly_netcdf/{0}.nc".format(yearlyList[0][:-11]))
        print "\n"
        retList = [makeArgument, myFilesToDelete]
        return retList
        
    def executeYearly(self, varType):
        """Execute the yearly argument returned from createYearly."""
        os.chdir(self.netcdfFolder)
        pids = []
        for year in xrange(1979, 2010, 1):
            retList = self.createYearly(varType, year)
            makeArgument = retList[0]
            print makeArgument
            p = subprocess.Popen(makeArgument)
            pids.append(p)
            for p in pids:
                p.wait()

def main():
    gribFolder = r"G:/gribs"
    hourlyFolder = r"G:/hourly_gribs"
    codeFolder = r"G:/code"
    netcdfFolder = r"G:/netcdf"
    prate = WritePrateBatch(gribFolder, hourlyFolder, codeFolder, netcdfFolder)
    #prate.createHourly()
    prate.executeYearly("prate")
    
if __name__ == "__main__":
    main()