#-------------------------------------------------------------------------------
# Name:        GribDownload.WriteNetCDF_Batch
# Purpose:
#
# Author:      Andrew Perry
#
# Created:     Apr 2, 2014
# Copyright:   (c) FM Global 2013
#-------------------------------------------------------------------------------
import os
import subprocess
import multiprocessing
import time

class createHourlyGrib(object):
    """Class that creates gribs that have annual data removed. 
    It then converts to netcdf and then concatenates them."""
    
    def __init__(self, gribFolder, middleFolder, codeFolder, netcdfFolder, hourlyNetcdfFolder):
        """Instantiate the class and variables."""
        self.gribFolder = gribFolder
        self.middleFolder = middleFolder
        self.codeFolder = codeFolder
        self.netcdfFolder = netcdfFolder
        self.hourlyNetcdfFolder = hourlyNetcdfFolder
        
    def createHourly(self, varType):
        """Open up 3 processes and remove daily averages and keep the remaining data."""
        start = time.clock()
        for year in xrange(1979, 2010, 1):
            for month in xrange(1, 13, 3):
                yearMonth1 = "{0}{1:02}".format(year, month)
                yearMonth2 = "{0}{1:02}".format(year, month+1)
                yearMonth3 = "{0}{1:02}".format(year, month+2)
                pids = []
                myFilesToDelete = []
                myFilesToNetCDF = []
                for myFile in os.listdir(self.gribFolder):
                    if varType in myFile and (yearMonth1 in myFile or yearMonth2 in myFile or yearMonth3 in myFile):
                        outputmyFile = "{0}Hourly{1}".format(myFile[:-5],myFile[-5:])
                        myFilesToDelete.append(os.path.join(r"E:/hourly_gribs/", outputmyFile))
                        myFilesToNetCDF.append(outputmyFile)
                        makeArgument = "wgrib2 /cygdrive/E/gribs/{0} \
                        -not_if \":anl:\" -grib ../hourly_gribs/{1}".format(myFile,outputmyFile)
                        os.chdir(self.codeFolder)
                        FNULL = open(os.devnull, 'w')
                        p = subprocess.Popen(makeArgument, stdout=FNULL, stderr=subprocess.STDOUT)
                        pids.append(p)
                for p in pids:
                    p.wait()
                pids = []
                for myFile2 in myFilesToNetCDF:
                    makeArgument2 = "wgrib2 /cygdrive/E/hourly_gribs/{0} \
                    -netcdf ../netcdf/{1}.nc".format(myFile2, myFile2[:-5])
                    FNULL = open(os.devnull, 'w')
                    p = subprocess.Popen(makeArgument2, stdout=FNULL, stderr=subprocess.STDOUT)
                    pids.append(p)
                for p in pids:
                    p.wait()
                print "FINISHED {0}".format(yearMonth3)
                print "It took {0} seconds".format(time.clock() - start)
        
    def createYearly(self, varType, year):
        """Create yearly netcdf myFiles by appending them together.
        varType can be q2m, tmp2m or wnd10m."""
        yearlyList = []
        myFilesToDelete = []
        for month in xrange(1, 13, 1):
            yearMonth = "{0}{1:02}".format(year, month)
            for myFile in os.listdir(self.netcdfFolder):
                if varType in myFile and yearMonth in myFile:
                    print myFile
                    yearlyList.append(myFile)
                    myFilesToDelete.append(os.path.join(self.netcdfFolder, myFile))
        makeArgument = "ncrcat {0} {1} {2} {3} {4} \
        {5} {6} {7} {8} {9} {10} {11} {12}".format(yearlyList[0], yearlyList[1], yearlyList[2],
                                                    yearlyList[3], yearlyList[4], yearlyList[5],
                                                    yearlyList[6], yearlyList[7], yearlyList[8],
                                                    yearlyList[9], yearlyList[10], yearlyList[11],
                                                    "../hourly_netcdf/{0}.nc".format(yearlyList[0][:-11]))

        print "\n"
        retList = [makeArgument, myFilesToDelete]
        return retList
        
    def executeYearly(self, varType):
        """Execute the yearly argument returned from createYearly."""
        os.chdir(self.netcdfFolder)
        pids = []
        for year in xrange(1979, 2010, 1):
            retList = self.createYearly(varType, year)
            makeArgument = retList[0]
            print makeArgument
            p = subprocess.Popen(makeArgument)
            pids.append(p)
            for p in pids:
                p.wait()
            #for myFile2 in retList[1]:
            #    os.remove(myFile2)
            
            
def main():
    gribFolder = r"E:/gribs"
    middleFolder = r"E:/hourly_gribs"
    codeFolder = r"E:/code"
    netcdfFolder = r"E:/netcdf"
    hourlyNetcdfFolder = r"E:/hourly_netcdf"
    hourlyGribObject = createHourlyGrib(gribFolder, middleFolder, codeFolder, netcdfFolder, hourlyNetcdfFolder)
    hourlyGribObject.createHourly("wnd10m")
    hourlyGribObject.executeYearly("wnd10m")
    
if __name__ == "__main__":
    main()