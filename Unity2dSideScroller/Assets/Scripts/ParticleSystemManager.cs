﻿using UnityEngine;
using System.Collections;

public class ParticleSystemManager : MonoBehaviour 
{
	public ParticleSystem[] particleSystems;
	public ParticleSystem explosionPrefab;
	public ParticleSystem fogFlow;

	// Use this for initialization
	void Start() 
	{
		GameEventManager.GameStart += GameStart;
		GameEventManager.GameOver += GameOver;
		GameEventManager.Death += Death;
		GameOver();
	}
	
	private void GameStart()
	{
		fogFlow.Stop();
		explosionPrefab.Clear(); // Completely removes the explosion
		for (int i = 0; i < particleSystems.Length; i++)
		{
			particleSystems[i].Clear();
			particleSystems[i].enableEmission = true;
		}
	}

	private void GameOver()
	{
		for (int i = 0; i < particleSystems.Length; i++)
		{
			particleSystems[i].enableEmission = false;
		}
	}

	private void Death()
	{
		explosionPrefab.Play();
	}
}
