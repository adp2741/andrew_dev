﻿using UnityEngine;
using System.Collections;

public class ComputerPlayer : MonoBehaviour 
{
	public float speed = 10.0f;
	private Transform ball;
	private float transY = 0.0f;
	private float randomPos = 0.0f;

	// Update is called once per frame
	void Update() 
	{
		if (GameObject.FindWithTag("Ball")) 
		{ 
			ball = GameObject.FindWithTag("Ball").transform; 
			if (ball.position.x > 0) 
			{
				randomPos = Random.Range(0.1f,1.0f);
				transY = Mathf.Lerp(transform.position.y, ball.position.y, speed * Time.deltaTime);
				rigidbody2D.transform.position = new Vector3(rigidbody2D.transform.position.x, transY, 0);
			}
			transY = Mathf.Clamp(transform.position.y * randomPos, Screen.height / -2, Screen.height / 2);
			rigidbody2D.transform.position = new Vector3(rigidbody2D.transform.position.x,  transY, 0);
		}
	}
}
