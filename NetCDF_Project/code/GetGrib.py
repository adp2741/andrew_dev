#-------------------------------------------------------------------------------
# Name:        GribDownload.GetGrib
# Purpose:
#
# Author:      Andrew Perry
#
# Created:     Mar 3, 2014
# Copyright:   (c) FM Global 2014
#-------------------------------------------------------------------------------
from ftplib import FTP
import os
import subprocess
import time

class GetFTP(object):
    """Class that opens up FTP and extracts data."""
    
    def __init__(self, ftpLocation, login, password, currentDirectory, listOfVariables, outputLoc):
        """Instantiate object with necessary inputs for connection."""
        self.ftpLocation = ftpLocation
        self.login = login
        self.password = password
        self.currentDirectory = currentDirectory
        self.listOfVariables = listOfVariables
        self.outputLoc = outputLoc
        self.gribFolder = os.path.join(self.outputLoc, "gribs")
        self.codeFolder = os.path.join(self.outputLoc, "code")
        
    def openConnection(self):
        """OPen FTP connection with proper credentials."""
        print "Opening connection..."
        self.ftp = FTP(self.ftpLocation, self.login, self.password)
        self.ftp.cwd(self.currentDirectory)
        
    def getDatasets(self, year, month):
        """Retrieve datasets from FTP."""
        dirList = self.ftp.nlst()
        for currDir in dirList:
            folderYearMonth = "{0}{1:02}".format(year, month)
            if currDir == folderYearMonth:
                print folderYearMonth
                self.ftp.cwd(currDir)
                #newDirList = self.ftp.nlst()
                for fileName in self.listOfVariables:
                    newName = fileName.replace("#temp#", currDir)
                    self.gribFolder = os.path.join(self.outputLoc, "gribs")
                    outputFile = os.path.join(self.gribFolder, newName)
                    tempFile = open(outputFile, "wb")
                    self.ftp.retrbinary("RETR "+ newName, tempFile.write)
                    tempFile.close
            
    def closeConnection(self):
        """Close FTP connection."""
        print "Closing connection..."
        self.ftp.close()
                            

def main():
    ftpLocation = "nomads.ncdc.noaa.gov"
    login = "anonymous"
    password = "username@nowhere.com"
    currentDirectory = "CFSR/HP_time_series"
    listOfVariables = ["prate.gdas.#temp#.grb2"]
    #listOfVariables = ["tmp2m.gdas.#temp#.grb2",
    #                   "q2m.gdas.#temp#.grb2", 
    #                   "wnd10m.gdas.#temp#.grb2"]
    #outputLoc = r"C:\Users\perryad\Desktop\Projects\FTP/"
    outputLoc = r"E:/"
    
    myFTP = GetFTP(ftpLocation, login, password, currentDirectory, listOfVariables, outputLoc)
    for year in xrange(1979, 2010, 1):
        for month in xrange(1,13,1):
            myFTP.openConnection()
            myFTP.getDatasets(year, month)
            myFTP.closeConnection()
            time.sleep(10)
    
    
if __name__ == "__main__":
    main()